package com.grainger.HCM.hcmedw.persistence.model;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ToDoItemRepository extends  PagingAndSortingRepository<ToDoItem, Long> {
    

}
