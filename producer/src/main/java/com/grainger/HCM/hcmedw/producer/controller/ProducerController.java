package com.grainger.HCM.hcmedw.producer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import com.grainger.HCM.hcmedw.producer.service.ProducerService;

@Component
@RestController
@RequestMapping("/publish")
public class ProducerController {
    /**
     * Create a simple producer controller that send a message to a topic
     */

    @Autowired
    private ProducerService producerService;

    @PostMapping("/{message}")
    public String sendMessage(@PathVariable("message") final String m) {
        try {
          return producerService.sendMessage(m);
        } catch (Exception e) {
           //throw new Exception("There was an error publishing the message: " + e.getMessage());
           return "There was an error publishing the message: " + e.getMessage();
        }
    }
}
