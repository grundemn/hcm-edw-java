package com.grainger.HCM.hcmedw.data.account.provider;

import com.grainger.starter.boot.test.data.provider.GenericDataLoader;
import com.grainger.HCM.hcmedw.data.account.model.AccountData;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("test-data.account")
public class AccountDataProperties extends GenericDataLoader<AccountData> {}
