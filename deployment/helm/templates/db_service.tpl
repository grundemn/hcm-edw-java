{{- if eq .Values.db.type "local" -}}
apiVersion: v1
kind: Service
metadata:
  name: {{ include "productcore-java-starterkit-db.fullname" . }}
  labels:
    {{- include "productcore-java-starterkit-db.labels" . | nindent 4 }}
spec:
  type: NodePort
  selector:
    {{- include "productcore-java-starterkit-db.selectorLabels" . | nindent 4 }}
  ports:
    - name: "tcp-broker-port"
      port: {{ .Values.db.port }}
      targetPort: 5432
{{- end }}
