{{/*
Expand the name of the chart.
*/}}
{{- define "productcore-java-starterkit.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "productcore-java-starterkit.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "productcore-java-starterkit.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "productcore-java-starterkit.labels" -}}
helm.sh/chart: {{ include "productcore-java-starterkit.chart" . }}
app: {{ include "productcore-java-starterkit.name" . }}
version: {{ default .Chart.AppVersion .Values.tag }}
{{ include "productcore-java-starterkit.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- include "productcore-java-starterkit.graingerLabels" . }}
{{- end }}

{{- define "productcore-java-starterkit-db.labels" -}}
helm.sh/chart: {{ include "productcore-java-starterkit.chart" . }}
app: {{ include "productcore-java-starterkit.name" . }}
version: {{ default .Chart.AppVersion .Values.tag }}
{{ include "productcore-java-starterkit-db.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- include "productcore-java-starterkit.graingerLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "productcore-java-starterkit.selectorLabels" -}}
app.kubernetes.io/name: {{ include "productcore-java-starterkit.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "productcore-java-starterkit-db.selectorLabels" -}}
app.kubernetes.io/name: {{ include "productcore-java-starterkit.name" . }}-db
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Grainger labels
*/}}
{{- define "productcore-java-starterkit.graingerLabels" -}}
{{- if .Values.teamname }}
grainger.com/managed-by-team: {{ .Values.teamname }}
{{- end -}}
{{- if .Values.reponame }}
grainger.com/repo: {{ .Values.reponame }}
{{- end -}}
{{- end -}}

{{/*
Image Repository
*/}}
{{- define "productcore-java-starterkit.container" -}}
{{- if .depObj.image.registry }}
    {{- .depObj.image.registry -}}
    /
{{- end -}}
{{- .depObj.image.repository -}}
:
{{- .depObj.image.tag | default .Chart.AppVersion }}
{{- end -}}

{{/*
Expand the name of the chart.
*/}}
{{- define "productcore-java-starterkit-db.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 60 | trimSuffix "-" }}-db
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "productcore-java-starterkit-db.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 60 | trimSuffix "-" }}-db
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 60 | trimSuffix "-" }}-db
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 60 | trimSuffix "-" }}-db
{{- end }}
{{- end }}
{{- end }}

{{ define "productcore-java-starterkit-db.dbServiceHost" -}}
{{- if eq .Values.db.type "local" -}}
{{- include "productcore-java-starterkit-db.fullname" . -}}
{{- else -}}
{{- .Values.db.serviceHost -}}
{{- end -}}
{{- end }}

{{ define "productcore-java-starterkit.productionSlot" -}}
{{- if eq .Values.productionSlot "blue" -}}
blue
{{- else -}}
green
{{- end -}}
{{- end }}

{{ define "productcore-java-starterkit.testSlot" -}}
{{- if eq .Values.productionSlot "blue" -}}
green
{{- else -}}
blue
{{- end -}}
{{- end }}
